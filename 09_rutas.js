let http = require('http');
let url = require('url');
let handlers = require('./lib/handlers');

let router = {
    '' : handlers.index,
    '/movie': handlers.movie,
    '/movie/score': handlers.movieScore,
    '/user/favourite': handlers.userFav,
    '/create': handlers.createNewFile
};

let server = http.createServer(function(req, res) {

    let parsedUrl = url.parse(req.url, true);
    let path = parsedUrl.pathname;

    let queryStringObject = parsedUrl.query;
    let method = req.method.toLowerCase();
    let headers = req.headers;

    let chosenHandler = typeof(router[path]) !== 'undefined' ? router[path] : handlers.notFound;

    let data = {
        'trimmedPath' : path,
        'queryStringObject' : queryStringObject,
        'method' : method,
        'headers' : headers
    };

    chosenHandler(data, (statusCode,payload) => {
        statusCode = typeof(statusCode) === 'number' ? statusCode : 200;

        payload = typeof(payload) == 'object'? payload : {};

        let payloadString = JSON.stringify(payload);

        res.setHeader('Content-Type', 'application/json');
        res.writeHead(statusCode);
        res.end(payloadString);
    });

    //res.end('¡¡¡Hola Mundo!!!');
    console.log("Petición recibida con el path: " + path
        + " con el método " + method + " y los query string",
        queryStringObject);
    console.log("Las cabeceras recibidas son: ", headers);
});

server.listen(3000, function(){
    console.log("El servidor está escuchando en el puerto 3000");
});
