let handlers = {};

handlers.index = function(data,callback){
    callback(200, {"Msg": "Hola Mundo"});
};

handlers.movie = function(data,callback){
    let acceptableMethods = ['get'];
    if(acceptableMethods.indexOf(data.method) > -1){
        callback(200, {"Msg": "La petición es válida"})
    } else {
        callback(405, {"Error": "La petición NO es válida"});
    }
};

handlers.movieScore = function(data,callback){
    let acceptableMethods = ['get'];
    if(acceptableMethods.indexOf(data.method) > -1){
        callback(200, {"Msg": "La petición es válida"})
    } else {
        callback(405, {"Error": "La petición NO es válida"});
    }
};

handlers.userFav = function(data,callback){
    let acceptableMethods = ['post', 'delete'];
    if(acceptableMethods.indexOf(data.method) > -1){
        callback(200, {"Msg": "La petición es válida"})
    } else {
        callback(405, {"Error": "La petición NO es válida"});
    }
};

handlers.notFound = function(data,callback){
    callback(404, {"Error": "La petición NO es válida"});
};


handlers.createNewFile = function(data,callback){
    console.log(data);
    callback(200, {"Creando nuevo fichero": "-----"+data.queryStringObject.name});
};


module.exports = handlers;
